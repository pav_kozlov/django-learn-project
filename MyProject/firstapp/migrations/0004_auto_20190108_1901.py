# Generated by Django 2.1.5 on 2019-01-08 16:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0003_auto_20190108_1415'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pizza',
            options={'ordering': ['name'], 'verbose_name': 'Пицца', 'verbose_name_plural': 'Пиццы'},
        ),
    ]
