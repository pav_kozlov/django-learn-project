from django.contrib import admin
from firstapp.models import *

# Register your models here.
admin.site.register(PizzaShop)
admin.site.register(Pizza)
