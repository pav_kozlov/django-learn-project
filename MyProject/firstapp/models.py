from django.db import models

# Create your models here.
#ПЕРВИЧНАЯ МОДЕЛЬ
class PizzaShop(models.Model):
    name = models.CharField(max_length=30, verbose_name='Пиццерия')
    description = models.TextField(verbose_name='Описание')
    rating = models.FloatField(default=0, verbose_name='Рейтинг')
    url = models.URLField(verbose_name='Интернет-адрес')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Пиццерия'
        verbose_name_plural = 'Пиццерии'

#ВТОРИЧНАЯ МОДЕЛЬ
class Pizza(models.Model):
    pizzashop = models.ForeignKey(PizzaShop, on_delete=models.CASCADE)
    name = models.CharField(max_length=30, verbose_name='Название пиццы')
    short_design = models.CharField(max_length=30, verbose_name='Краткое описание')
    price = models.IntegerField(default=0, verbose_name='Цена')
    photo = models.ImageField('Фото', blank=True, default='', upload_to='firstapp/photos')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Пицца'
        verbose_name_plural = 'Пиццы'
        ordering = ['name']
