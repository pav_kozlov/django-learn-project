from django.urls import path
from testurl.views import home

urlpatterns = [
    path('user/<int:day>', home, name='home'),
]
